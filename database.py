class Database:
    def __init__(self, mysql):
        self.mysql = mysql
        self.db = self.mysql.connection
        self.cur = self.db.cursor()

    # ---------------------- GENERAL DATABASE -----------------------
    def get_all(self, tb_name):
        # self.cur.execute('SELECT * FROM %s', (str(tb_name),))
        self.cur.execute('SELECT * FROM ' + tb_name + '')
        data = self.cur.fetchall()
        return data

    def get_all_count(self, tb_name, id):
        self.cur.execute('SELECT * FROM ' + tb_name + ' WHERE id=' + id)
        data = self.cur.fetchall()
        count = 0
        for i in data:
            count = count + 1
        return count

    def get_one_product(self, tb_name, product_id):
        self.cur.execute('SELECT * FROM ' + tb_name + ' WHERE id=' + product_id)
        data = self.cur.fetchone()

        return data

    def filter(self, tb_name, column, word):
        self.cur.execute('SELECT * FROM '+tb_name+' WHERE  '+column+' LIKE "%' + word + '%" ')
        data = self.cur.fetchall()
        for i in data:
            print(i)
        return data

    def delete_filter(self, tb_name, column, word):
        self.cur.execute('DELETE FROM %s WHERE  name LIKE "%' + word + '%"', (tb_name, column,))
        self.db.commit()

    def delete(self, tb_name, my_id):
        self.cur.execute('DELETE FROM ' + tb_name + ' WHERE id =' + my_id)
        self.db.commit()

    def insert_subscriber(self, email, date):
        self.cur.execute(
            'INSERT INTO subscribers (email, date) VALUES (%s,%s)',
            (email, date,))
        self.db.commit()

    def insert_gallery(self, image, date):
        self.cur.execute(
            'INSERT INTO blog (image, date) VALUES (%s,%s,)',
            (image, date))
        self.db.commit()
        # Users_table function

    # def update_password(self, email, new_password):
    #     self.cur.execute('UPDATE users SET password = %s WHERE email = %s', (new_password, email,))
    #     self.db.commit()

    # -------------------------- END GENERAL ---------------------------

    # ----------------- PRODUCT DATABASE SECTION ------------------------

    def insert_product(self, product_name, product_price, description, key_points, p_condition, category, payment_type, product_type, product_image,  date):
        self.cur.execute(
            'INSERT INTO products (product_name, product_price, description, key_points, p_condition, category, payment_type, product_type, product_image, date) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
            (product_name, product_price, description, key_points, p_condition, category, payment_type, product_type, product_image,  date))
        self.db.commit()

    def update_product(self, ID, product_name, product_price, description, key_points, p_condition, category, payment_type, product_type, product_image,  date):
        self.cur.execute('UPDATE products SET '
                         'product_name=%s, product_price=%s, description=%s, key_points=%s, p_condition=%s, category=%s, payment_type=%s, product_type=%s, product_image=%s, date=%s WHERE id=%s',
                         (product_name, product_price, description, key_points, p_condition, category, payment_type, product_type, product_image,  date, ID))
        self.db.commit()

    # ------------------------BLOG DATABASE SECTION---------------------------------

    def insert_blog(self, title, paragraph1, paragraph2, paragraph3, paragraph4, blog_image, date):
        self.cur.execute(
            'INSERT INTO blog (title, paragraph_one, paragraph_two, paragraph_three, paragraph_four, blog_image, date ) VALUES (%s,%s,%s,%s,%s,%s,%s)',
            (title, paragraph1, paragraph2, paragraph3, paragraph4, blog_image, date))
        self.db.commit()

    def update_blog(self, ID, title, paragraph1, paragraph2, paragraph3, paragraph4, blog_image, date):
        self.cur.execute('UPDATE blog SET '
                         'title=%s, paragraph_one=%s, paragraph_two=%s, paragraph_three=%s, paragraph_four=%s, blog_image=%s, date=%s WHERE ID=%s',
                         (title, paragraph1, paragraph2, paragraph3, paragraph4, blog_image, date, ID))
        self.db.commit()

    # def insert_click(self, product_id, clicks):
    #     self.cur.execute('UPDATE product SET clicks=%s WHERE id=%s', (clicks, product_id,))
    #     self.db.commit()
    #
    # def insert_view(self, product_id, view):
    #     self.cur.execute('UPDATE product SET views=%s WHERE id=%s', (view, product_id,))
    #     self.db.commit()

    # -------------------------- END BUSINESS ----------------------------

    # def get_highest(self, b_id, view_or_click):
    #
    #     if view_or_click == 'click':
    #         self.cur.execute('SELECT * FROM product WHERE b_id=%s', (b_id,))
    #         products = self.cur.fetchall()
    #         highest = 0
    #         result = ''
    #         for product in products:
    #             checker = product.get('clicks')
    #             if highest <= checker:
    #                 highest = checker
    #                 result = product
    #         return result
    #     else:
    #         self.cur.execute('SELECT * FROM product WHERE b_id=%s', (b_id,))
    #         products = self.cur.fetchall()
    #         highest = 0
    #         result = ''
    #         for product in products:
    #             checker = product.get('views')
    #             if highest <= checker:
    #                 highest = checker
    #                 result = product
    #         return result
