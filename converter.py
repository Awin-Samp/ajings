
def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData


def write_file(data, filename):
    # Convert binary data to proper format and write it on Hard Disk
    with open(filename, 'wb') as file:
        file.write(data)
        return file

# bdata = convertToBinaryData('Ajings.png')
# print(bdata)
#
# write_file(bdata, 'latest.png')