import os

from flask import Flask, render_template, request, redirect, session
from flask_mysqldb import MySQL
from datetime import date

from werkzeug.utils import secure_filename

import database

app = Flask(__name__)
app.secret_key = "my_secret123"

#  DATABASE CONFIGURATION
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_DB'] = 'ajingsdb'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

UPLOAD_FOLDER_BLOG = './static/images/blogs'
UPLOAD_FOLDER_PRODUCT = './static/images/products'
app.config['UPLOAD_FOLDER_BLOG'] = UPLOAD_FOLDER_BLOG
app.config['UPLOAD_FOLDER_PRODUCT'] = UPLOAD_FOLDER_PRODUCT
mysql = MySQL(app)

today = date.today()
full_date = today.strftime("%B %d, %Y")


@app.route('/')
def index():
    try:
        products = database.Database(mysql).get_all('products')
        blogs = database.Database(mysql).get_all('blog')
        # print(products, blogs)
        return render_template('index.html', products=products, blogs=blogs)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/gallery')
def gallery():
    return render_template('gallery.html')


@app.route('/blog')
def blog():
    try:
        blogs = database.Database(mysql).get_all('blog')
        return render_template('blog.html', blogs=blogs)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'


@app.route('/single-blog')
def single_blog():
    params = request.args.to_dict()
    ID = params.get('id')
    try:
        blogs = database.Database(mysql).get_all('blog')
        blog = database.Database(mysql).get_one_product('blog', ID)
        return render_template('single-blog.html', blogs=blogs, blog=blog)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'
    return render_template('single-blog.html')


@app.route('/category')
def category():
    try:
        products = database.Database(mysql).get_all('products')
        return render_template('category.html', products=products)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'


@app.route('/single-category')
def single_category():
    params = request.args.to_dict()
    category = params.get('category')
    print('PRoduct category: ', category)
    try:
        products = database.Database(mysql).filter(tb_name='products', column='category', word=category)
        print(products)
        return render_template('single-category.html', products=products, category=category)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'


@app.route('/single')
def single():
    params = request.args.to_dict()
    ID = params.get('id')
    try:
        products = database.Database(mysql).get_all('products')
        product = database.Database(mysql).get_one_product('products', ID)
        return render_template('single.html', product=product, products=products)
    except Exception as e:
        print('error here ', e)
        return '404 ERROR'


# Dashboard Section

@app.route('/dashboard')
def dashboard():
    if 'email' in session:
        email = session['email']
        print('sessoin: ', email)

        try:
            db = database.Database(mysql)
            product_total = db.get_all_count('products', 'id')
            blog_total = db.get_all_count('blog', 'id')
            subscriber_total = db.get_all_count('subscribers', 'id')
            subscribers = db.get_all('subscribers')
            print(product_total, blog_total, subscriber_total)

            return render_template('dashboard.html', email=email, product_total=product_total, blog_total=blog_total,
                                   subscriber_total=subscriber_total, subscribers=subscribers)

        except Exception as e:
            print(e)
            return redirect('/')
    else:
        return redirect('/')


@app.route('/login', methods=['POST', 'GET'])
def loginn():
    if request.method == 'GET':
        return render_template('login.html')

    if request.method == 'POST':

        email = request.form.get('email')
        password = request.form.get('password')

        try:
            login = database.Database(mysql).get_all('login')
            for one in login:
                db_email = one.get('email')
                db_password = one.get('password')

                if db_email == email and db_password == password:
                    print('login successful')
                    session['email'] = email
                    return redirect('/dashboard')
                else:
                    return redirect('/dashboard')

        except Exception as e:
            print(e)
            return redirect('/login')


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/login')


@app.route('/subscribers')
def messages():
    if 'email' in session:
        email = session['email']

        try:
            subscribers = database.Database(mysql).get_all('subscribers')
            return render_template('subscribers.html', subscribers=subscribers, email=email)
        except Exception as e:
            print(e)
    return render_template('subscribers.html')


@app.route('/subscribe', methods=['POST'])
def subscribe():
    email = request.form.get('email')
    try:
        database.Database(mysql).insert_subscriber(email=email, date=today)
        return redirect('/')
    except Exception as e:
        print(e)
        return redirect('/')


# Blogs section
@app.route('/single-dblog')
def singleBlog():
    if 'email' in session:
        email = session['email']
        params = request.args.to_dict()
        ID = params.get('id')
        try:
            blog = database.Database(mysql).get_one_product('blog', ID)
            return render_template('single-dblog.html', email=email, blog=blog)
        except Exception as e:
            print('error here ', e)
        return render_template('single-dblog.html')
    else:
        return render_template('single-dblog.html')


@app.route('/all-blogs')
@app.route('/dblog')
def dblog():
    if 'email' in session:
        email = session['email']
        try:
            blogs = database.Database(mysql).get_all('blog')
            return render_template('d-blog.html', blogs=blogs, email=email)
        except Exception as e:
            print('error here ', e)
            return redirect('/')
    else:
        return redirect('/')


@app.route('/upload-blog', methods=['POST', 'GET'])
def uploadBlog():
    if request.method == 'GET':
        if 'email' in session:
            email = session['email']
            print('ID0: ', email)
            return render_template('upload-blog.html', email=email)
        else:
            return redirect('/')

    if request.method == 'POST':
        title = request.values.get('title')
        paragraph_one = request.values.get('paraOne')
        paragraph_two = request.values.get('paraTwo')
        paragraph_three = request.values.get('paraThree')
        paragraph_four = request.values.get('paraFour')
        image = request.files['image']
        blog_image = secure_filename(image.filename)
        print(title, paragraph_one, paragraph_four, paragraph_three, paragraph_two, blog_image, today)
        try:
            db = database.Database(mysql)
            db.insert_blog(title=title, paragraph1=paragraph_one, paragraph2=paragraph_two, paragraph3=paragraph_three, paragraph4=paragraph_four, blog_image=blog_image, date=today)
            print('database established.....')
            image.save(os.path.join(app.config['UPLOAD_FOLDER_BLOG'], blog_image))
        except Exception as e:
            print(e)
        return redirect('/dashboard')


@app.route('/update-blog', methods=['GET', 'POST'])
def updateBlog():
    params = request.args.to_dict()
    ID = params.get('id')

    if request.method == 'GET':
        if 'email' in session:
            email = session['email']
            print('get ', ID)
            try:
                blog = database.Database(mysql).get_one_product('blog', ID)
                return render_template('update-blog.html', blog=blog, email=email)

            except Exception as e:
                print(e)
                return redirect('/')
        else:
            return redirect('/')

    if request.method == 'POST':
        print('post ', ID)
        title = request.values.get('title')
        paragraph_one = request.values.get('paraOne')
        paragraph_two = request.values.get('paraTwo')
        paragraph_three = request.values.get('paraThree')
        paragraph_four = request.values.get('paraFour')
        image = request.files['image']
        blog_image = secure_filename(image.filename)
        print(title, paragraph_one, paragraph_four, paragraph_three, paragraph_two, blog_image, today)
        try:
            db = database.Database(mysql)
            db.update_blog(ID=ID, title=title, paragraph1=paragraph_one, paragraph2=paragraph_two, paragraph3=paragraph_three,
                           paragraph4=paragraph_four, blog_image=blog_image, date=today)
            print('database established.....')
            image.save(os.path.join(app.config['UPLOAD_FOLDER_BLOG'], blog_image))
            return redirect('/dashboard')
        except Exception as e:
            print(e)
            return redirect('/dashboard')


@app.route('/delete-blog', methods=['GET'])
def deleteBlog():
    if request.method == 'GET':
        params = request.args.to_dict()
        ID = params.get('id')
        if 'email' in session:
            try:
                database.Database(mysql).delete('blog', ID)
                return redirect('/all-blogs')
            except Exception as e:
                print(e)
                return redirect('/dashboard')
        else:
            return redirect('/')


# Product section
@app.route('/single-product')
def singleProduct():
    if request.method == 'GET':
        if 'email' in session:
            email = session['email']
            params = request.args.to_dict()
            ID = params.get('id')
            try:
                product = database.Database(mysql).get_one_product('products', ID)
                return render_template('single-product.html', product=product,  email=email)
            except Exception as e:
                print('error here ', e)
                return '404 ERROR'
        else:
            return redirect('/')


@app.route('/upload-product', methods=['POST', 'GET'])
def uploadProduct():
    if request.method == 'GET':
        if 'email' in session:
            email = session['email']
            print('ID0: ', email)
            return render_template('upload-product.html',email=email)
        else:
            return redirect('/')

    if request.method == 'POST':
        product_name = request.values.get('product_name')
        product_price = request.values.get('product_price')
        description = request.values.get('description')
        key_points = request.values.get('key_points')
        condition = request.values.get('condition')
        category1 = request.values.get('category')
        product_type = request.values.get('product_type')
        # payment_type_cash = request.values.get('payment_type_cash')
        # payment_type_momo = request.values.get('payment_type_momo')
        # payment_type_visa = request.values.get('payment_type_visa')
        image = request.files['product_image']

        product_image = secure_filename(image.filename)
        # print(title, paragraph_one, paragraph_four, paragraph_three, paragraph_two, blog_image, today)
        try:
            db = database.Database(mysql)
            db.insert_product(product_name=product_name, product_price=product_price, description=description, product_type=product_type,
                              key_points=key_points, p_condition=condition, category=category1, payment_type='Cash, Mobile Money,Visa Card',
                              product_image=product_image, date=today)
            print('database established.....')
            image.save(os.path.join(app.config['UPLOAD_FOLDER_PRODUCT'], product_image))
        except Exception as e:
            print(e)
        return redirect('/dashboard')


@app.route('/all-product', methods=['GET'])
def allProduct():
    if request.method == 'GET':
        if 'email' in session:
            email = session['email']

            try:
                products = database.Database(mysql).get_all('products')
                return render_template('all-products.html', email=email, products=products)
            except Exception as e:
                print(e)
                return redirect('/')
        else:
            return redirect('/')


# FOR UPDATING PRODUCT
@app.route('/up', methods=['POST',  'GET'])
def updateProduct():
    params = request.args.to_dict()
    ID = params.get('id')
    print(ID)

    if request.method == 'GET':
        if 'email' in session:
            email = session['email']
            print('get ', ID)
            return render_template('update-product.html', email=email)
        else:
            return redirect('/')

    if request.method == 'POST':
        print('my post id: ', ID)
        product_name = request.values.get('product_name')
        product_price = request.values.get('product_price')
        description = request.values.get('description')
        key_points = request.values.get('key_points')
        condition = request.values.get('condition')
        category1 = request.values.get('category')
        product_type = request.values.get('product_type')
        # payment_type_cash = request.values.get('payment_type_cash')
        # payment_type_momo = request.values.get('payment_type_momo')
        # payment_type_visa = request.values.get('payment_type_visa')
        image = request.files['product_image']

        product_image = secure_filename(image.filename)
        # print(title, paragraph_one, paragraph_four, paragraph_three, paragraph_two, blog_image, today)
        try:
            db = database.Database(mysql)
            db.update_product(ID=ID, product_name=product_name, product_price=product_price, description=description, product_type=product_type,
                              key_points=key_points, p_condition=condition, category=category1, payment_type='Cash, Mobile Money,Visa Card',
                              product_image=product_image, date=today)
            print('Updated database established.....')
            image.save(os.path.join(app.config['UPLOAD_FOLDER_PRODUCT'], product_image))
        except Exception as e:
            print(e)
        return redirect('/dashboard')


@app.route('/delete-product', methods=['GET'])
def deleteProduct():
    if request.method == 'GET':
        params = request.args.to_dict()
        ID = params.get('id')

        if 'email' in session:
            try:
                database.Database(mysql).delete('products', ID)
                return redirect('/all-product')
            except Exception as e:
                print(e)
                return redirect('/dashboard')
        else:
            return redirect('/')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=1000, debug=True)
